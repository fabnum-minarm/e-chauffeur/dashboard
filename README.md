# eChauffeur dashboard app
[![pipeline status](https://gitlab.com/fabnum-minarm/e-chauffeur/dashboard/badges/develop/pipeline.svg)](https://gitlab.com/fabnum-minarm/e-chauffeur/dashboard/-/commits/develop)

> eChauffeur administration application

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
```
