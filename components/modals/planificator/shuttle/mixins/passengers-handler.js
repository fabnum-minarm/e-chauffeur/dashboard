import { CANCEL_REQUESTED_CUSTOMER } from '@fabnumdef/e-chauffeur_lib-vue/api/status/states';

export default () => ({
  computed: {
    capacity() {
      if (this.shuttle.car && this.shuttle.car.model && this.shuttle.car.model.capacity) {
        return this.shuttle.car.model.capacity;
      }
      return 0;
    },
  },
  methods: {
    async cancelPassenger(passenger) {
      const { data: { passengers } } = await this.$api.query('shuttles')
        .setMask('passengers')
        .mutatePassenger(this.shuttle.id, CANCEL_REQUESTED_CUSTOMER, passenger.id);
      this.shuttle.passengers = passengers;
    },
    updatePassengers(passengers) {
      this.shuttle.passengers = passengers;
    },
  },
});
