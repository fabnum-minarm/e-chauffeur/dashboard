/** API generic licenses * */
export const CAR_LICENSE = 'B';
export const PUBLIC_TRANSPORT_LICENSE = 'D';
export const HEAVY_WEIGHT_LICENSE = 'C';
export const SUPER_HEAVY_WEIGHT_LICENSE = 'C1';
export const DRIVING_LICENSES = [
  CAR_LICENSE,
  PUBLIC_TRANSPORT_LICENSE,
  HEAVY_WEIGHT_LICENSE,
  SUPER_HEAVY_WEIGHT_LICENSE,
];

/** Application specific matches * */
export const DISPLAY_CAR_LICENSE = 'VL';
export const DISPLAY_PUBLIC_TRANSPORT_LICENSE = 'TC';
export const DISPLAY_HEAVY_WEIGHT_LICENSE = 'PL';
export const DISPLAY_SUPER_HEAVY_WEIGHT_LICENSE = 'SPL';

export function convertIncomingLicenseName(license) {
  switch (license) {
    case CAR_LICENSE:
      return DISPLAY_CAR_LICENSE;
    case PUBLIC_TRANSPORT_LICENSE:
      return DISPLAY_PUBLIC_TRANSPORT_LICENSE;
    case HEAVY_WEIGHT_LICENSE:
      return DISPLAY_HEAVY_WEIGHT_LICENSE;
    case SUPER_HEAVY_WEIGHT_LICENSE:
      return DISPLAY_SUPER_HEAVY_WEIGHT_LICENSE;
    default:
      return license;
  }
}

export function convertOutcomingLicenseName(license) {
  switch (license) {
    case DISPLAY_CAR_LICENSE:
      return CAR_LICENSE;
    case DISPLAY_PUBLIC_TRANSPORT_LICENSE:
      return PUBLIC_TRANSPORT_LICENSE;
    case DISPLAY_HEAVY_WEIGHT_LICENSE:
      return HEAVY_WEIGHT_LICENSE;
    case DISPLAY_SUPER_HEAVY_WEIGHT_LICENSE:
      return SUPER_HEAVY_WEIGHT_LICENSE;
    default:
      return license;
  }
}

export const convertArrayOfLicenses = (
  licenses = [],
  converter = convertIncomingLicenseName,
) => licenses.map((license) => converter(license));
