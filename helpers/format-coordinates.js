export default function formatCoordinatesHelper(array) {
  return array.map((item) => item.toString().replace(',', '.'));
}
