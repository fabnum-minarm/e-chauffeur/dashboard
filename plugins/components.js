import Vue from 'vue';
import ecField from '~/components/form/field.vue';
import ecChoice from '~/components/form/choice.vue';
import ecButton from '~/components/form/button.vue';

// Here, we will import only components used widely on the application
Vue.component('EcField', ecField);
Vue.component('EcChoice', ecChoice);
Vue.component('EcButton', ecButton);
