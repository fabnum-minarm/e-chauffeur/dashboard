import Vue from 'vue';
import Datetime from 'vue2-datepicker';

Vue.component('DateTime', Datetime);
